<?php
/**
 * Реализовать проверку заполнения обязательных полей формы в предыдущей
 * с использованием Cookies, а также заполнение формы по умолчанию ранее
 * введенными значениями.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');
$ability_labels = ['god' => 'Бессмертие', 'fly' => 'Полет', 'idclip' => 'Прохождение сквозь стены', 'fireball' => 'Файрболлы'];

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();

  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    // Если есть параметр save, то выводим сообщение пользователю.
    $messages[] = 'Спасибо, результаты сохранены.';
  }

  // Складываем признак ошибок в массив.
  $errors = array();
  $errors['fio'] = !empty($_COOKIE['fio_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['year'] = !empty($_COOKIE['year_error']);
  $errors['legs'] = !empty($_COOKIE['legs_error']);
  $errors['sex'] = !empty($_COOKIE['sex_error']);
  $errors['abilities'] = !empty($_COOKIE['abilities_error']);
  $errors['biography'] = !empty($_COOKIE['biohraphy_error']);
  $errors['check1'] = !empty($_COOKIE['check1_error']);

  // TODO: аналогично все поля.

  // Выдаем сообщения об ошибках.
  
  
  
  if ($errors['fio']) {
    // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('fio_error', '', 100000);
    if($_COOKIE['fio_error'] == '1') 
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните имя.</div>';
	else $messages[]='<div class="error"> Недопустимые символы в имени.</div>';
}
  if ($errors['email']) {
    // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('email_error', '', 100000);
    if($_COOKIE['email_error'] == '1') 
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните email.</div>';

}
   
if ($errors['year']) {
    setcookie('year_error', '', 100000);
     if($_COOKIE['fio_error'] == '1') 
    $messages[] = '<div class="error">Заполните год.</div>';
	else $messages[]='<div class="error"> Недопустимые символы в году.</div>';
  }


if ($errors['sex']) {
    setcookie('sex_error', '', 100000);
    $messages[] = '<div class="error"> Не выбран пол </div>';
  }


if ($errors['abilities']) {
    setcookie('abilities_error', '', 100000);
    $messages[] = '<div class="error"> Не выбрана супер </div>';
  }


if ($errors['legs']) {
    setcookie('legs_error', '', 100000);
    $messages[] = '<div class="error"> Не выбраны конечности </div>';
}

if ($errors['biography']) {
    setcookie('biography_error', '', 100000);
    $messages[] = '<div class="error"> Пустая биография </div>';
}

if ($errors['check1']) {
    setcookie('check1_error', '', 100000);
    $messages[] = '<div class="error"> Необходимо ваше согласие! </div>';
}



  // TODO: тут выдать сообщения об ошибках в других полях.

  // Складываем предыдущие значения полей в массив, если есть.
  $values = array();
 if (isset($_COOKIE['fio_value']))
  $values['fio'] = empty($_COOKIE['fio_value']) || !preg_match('/^[а-яА-Я ]+$/u', $_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
  else $values['fio'] ='';

  $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
$values['year'] = empty($_COOKIE['year_value']) ? '' : $_COOKIE['year_value'];
$values['sex'] = empty($_COOKIE['sex_value']) ? '' : $_COOKIE['sex_value'];
$values['legs'] = empty($_COOKIE['legs_value']) ? '' : $_COOKIE['legs_value'];
$values['abilities'] = empty($_COOKIE['abilities_value']) ? '' : $_COOKIE['abilities_value'];
$values['biography'] = empty($_COOKIE['biography_value']) ? '' : $_COOKIE['biography_value'];
$values['check1'] = empty($_COOKIE['check1_value']) ? '' : $_COOKIE['check1_value'];
  // TODO: аналогично все поля.

  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
  // Проверяем ошибки.
  $errors = FALSE;
  
  if (empty($_POST['fio'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('fio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else if (!preg_match('/^[а-яА-Я ]+$/u', $_POST['fio'])) 
    // Выдаем куку на день с флажком об ошибке в поле fio.
   { setcookie('fio_error', '2', time() + 24 * 60 * 60);
    $errors = TRUE;}
    else {
    // Сохраняем ранее введенное в форму значение на месяц.
    
    setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);}
  
  
  if (empty($_POST['email']) ) {
		setcookie('email_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	else {
		setcookie('email_value', $_POST['email'], time() + 365 * 24 * 60 * 60);
	}

	if (empty($_POST['year'])) {
		setcookie('year_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	else { 
	    $year = $_POST['year'];
        if (!(is_numeric($year) && intval($year) >= 1900 && intval($year) <= 2020)) 
        setcookie('year_error', '2', time() + 24 * 60 * 60);
        	
    	else
		setcookie('year_value', $_POST['year'], time() + 365 * 24 * 60 * 60);
}

	if ( empty($_POST['sex'])) {
		setcookie('sex_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	else {
		setcookie('sex_value', $_POST['sex'], time() + 365 * 24 * 60 * 60);
	}
	

	if (empty($_POST['legs'])) {
		setcookie('legs_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	else {
		setcookie('legs_value', $_POST['legs'], time() + 365 * 24 * 60 * 60);
	}

	if (empty($_POST['abilities'])) {
		setcookie('abilities_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	else 
	{
		$abilities =serialize($_POST['abilities']);
		setcookie('abilities_value', $abilities, time() + 365 * 24 * 60 * 60);
	}

$ability_data = array_keys($ability_labels);
$ability_insert = [];
$abilities = $_POST['abilities'];
  foreach ($ability_data as $ability)
  
{ $ability_insert[$ability] = in_array($ability, $abilities) ? 1 : 0;}

if (empty($_POST['biography'])) {
		setcookie('biography_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	else {
		setcookie('biography_value', $_POST['biography'], time() + 365 * 24 * 60 * 60);
	}

	if (empty($_POST['check1'])) {
		setcookie('check1_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	else {
		$check1 = $_POST['check1'];
		setcookie('check1_value', $check1, time() + 365 * 24 * 60 * 60);
	}


// *************
// TODO: тут необходимо проверить правильность заполнения всех остальных полей.
// Сохранить в Cookie признаки ошибок и значения полей.
// *************



  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 100000);
     setcookie('email_error', '', 100000);
        setcookie('year_error', '', 100000);
        setcookie('sex_error', '', 100000);
        setcookie('legs_error', '', 100000);
        setcookie('abilities_error', '', 100000);
        setcookie('biography_error', '', 100000);
        setcookie('check1_error', '', 100000);

    // TODO: тут необходимо удалить остальные Cookies.
  }
  
  
$user = 'u15851';
$pass = '6322046';
$db = new PDO('mysql:host=localhost;dbname=u15851', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

// Подготовленный запрос. Не именованные метки.
try {
  $stmt = $db->prepare("INSERT INTO application SET name = ?, email = ?, year = ?, sex = ?, legs = ?, ability_god = ?, ability_fly = ?, ability_idclip = ?, ability_fireball = ?, biography = ?, check1 = ?");
  $stmt->execute([$_POST['fio'], $_POST['email'], intval($year), $_POST['sex'], $_POST['legs'], $ability_insert['god'], $ability_insert['fly'], $ability_insert['idclip'], $ability_insert['fireball'], $_POST['biography'], $_POST['check1']]);
}

catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}



  // Сохранение в XML-документ.
  // ...

  // Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');

  // Делаем перенаправление.
  header('Location: index.php');
}
